def upperLowerDigits(string):
    upper = sum(1 for c in string if c.isupper())
    lower = sum(1 for c in string if c.islower())
    numOfNumbers = sum(1 for c in string if c.isnumeric())
    return (upper, lower, numOfNumbers)

def main():
    print('Veliko slovo, malo slovo, broj: ', upperLowerDigits("asdf98CXX21grrr"))

if __name__ == "__main__":
    main()