def evens(list):
    sum = 0
    for i in list:
        if i % 2 == 0:
            sum += 1
    return sum

def strListToInt():
    string = str(input('Unesite brojeve koji su odvojeni zarezom: '))
    lista = string.split(',')
    lista = [int(i) for i in lista]
    return lista

def main():
    lista = strListToInt()
    print('Broj parnih brojeva unutar unesene liste: ', evens(lista))

if __name__ == "__main__":
    main()