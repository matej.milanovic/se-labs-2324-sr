def centeredAverage(list):
    min = list[0]
    max = list[0]
    for i in list:
        if i > max: max = i
        if i < min: min = i

    suma = 0
    flag = 0
    for i in list:
        suma += i
    suma = suma - min - max
    return suma / (len(list) - 2)

def strListToInt():
    string = str(input('Unesite brojeve koji su odvojeni zarezom: '))
    lista = string.split(',')
    lista = [int(i) for i in lista]
    return lista

def main():
    lista = strListToInt()
    print('Centered average: ', centeredAverage(lista))

if __name__ == "__main__":
    main()