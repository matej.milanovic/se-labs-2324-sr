from multiprocessing.sharedctypes import Value
from unicodedata import name
from query_handler_base import QueryHandlerBase
import random
import requests
import json

class JokesHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "jokes" in query:
            return True
        return False

    def process(self, query):
        names = query.split()
        term = names
        
        try:
            result = self._api()
            text = result("Hope you found what you were looking for")
            self.ui.say(f"Your word was {term}")
        except:
            self.ui.say("Oh no! Ther was an error trying to conect urban dictionary api ")
            self.ui.say("try something else ")



    def call_api(self):
        url = "https://world-of-jokes1.p.rapidapi.com/v1/jokes/categories"

        queryString = {}

        headers = {
	        "X-RapidAPI-Key": "b6d3071c38msh3aa1475a21ef920p15a959jsn21bad126dc9b",
	        "X-RapidAPI-Host": "world-of-jokes1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        print(response.json())