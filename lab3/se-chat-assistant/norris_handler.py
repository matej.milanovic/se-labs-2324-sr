from multiprocessing.sharedctypes import Value
from query_handler_base import QueryHandlerBase
import random
import requests
import json

class NorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "norris" in query:
            return True
        return False

    def process(self, query):
        
        try:
            result = self._api()
            text = result("value")
            self.ui.say(f"{text}")
        except:
            self.ui.say("I can handle this request but I won't")



    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        querystring = {}

        headers = {
            "accept": "application/json",
            "X-RapidAPI-Key": "b6d3071c38msh3aa1475a21ef920p15a959jsn21bad126dc9b",
	        "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        print(response.json())