import json
from json import JSONEncoder

class Employee:

    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __repr__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"

class MyEncoder(JSONEncoder):
    def default(self, obj):
        return obj.__dict__   

class Company:

    def __init__(self, name):
        self.name = name
        self.employees = []

    def __repr__(self):
        return f"{self.name} ({len(self.employees)} employees)"

    def employ(self, name, title, age, office):
        new_employee = Employee(name, title, age, office)
        self.employees.append(new_employee)

    def fire(self, name):
        for e in self.employees:
            if e.name == name:
                self.employees.remove(e)

    def load_from_json(self, path_to_json):
        with open(".\lab2\ex4-employees.json", "r", encoding="utf-8") as f:
            epmloyees_json = json.load(f)
        for e in epmloyees_json:
            self.employees.append(Employee(e['employee'], e['title'], e['age'], e['office']))

    def save_to_json(self, path_to_json):
        with open(path_to_json, 'w') as f:
            json.dumps(self.employees, cls=MyEncoder)
        pass

    def print_employees(self):
        for e in self.employees:
            print(e)

        """Print all employees to stdout in format:
        Company name
        ----------------
        1. Employee name (age), job_title @ office
        2. Employee name (age), job_title @ office
        ..."""
        pass


def main():
    nike = Company("Nike")

    nike.employ("Homer Simpson", "CEO", 44, "Lobby")
    nike.employ("Marge Simpson", "CTO", 33, "Lobby")
    nike.print_employees()

    nike.fire("Homer Simpson")
    nike.print_employees()

    print('----------------------------------------------------------------------')

    # Implement load_from_json, save_to_json and print_employees methods
    # Then uncomment the implemented methods
    adidas = Company("Adidas")
    adidas.load_from_json("ex4-employees.json")
    # After loading from json, adidas should have all the employees from
    # json file
    # Print employees should now print all the employees
    adidas.print_employees()

    adidas.employ("Homer Simpson", "CEO", 44, "Lobby")
    adidas.employ("Marge Simpson", "CTO", 33, "Lobby")
    adidas.employ("Bart Simpson", "CEO", 44, "Lobby")
    adidas.employ("Lisa Simpson", "CTO", 33, "Lobby")
    adidas.print_employees()

    adidas.fire("Homer Simpson")
    adidas.fire("Marge Simpson")
    adidas.print_employees()

    # Saving employees db to a new file, the file should now have 2 more
    # employees (Bart and Lisa, since Homer and Marge were fired)
    adidas.save_to_json("ex6-employees.json")

if __name__ == "__main__":
    main()